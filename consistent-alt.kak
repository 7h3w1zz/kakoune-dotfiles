## Bindings to correct some of the inconsistencies with the
## convention of the alt key changing the direction of a
## movement/operation

# Word movement. Instead of W for WORD, use b for "big word".
map -docstring "select big word"                global normal b     <a-w>
map -docstring "select big word, extends"       global normal B     <a-W>
map -docstring "select word backwards"          global normal <a-w> b
map -docstring "select word backwards, extends" global normal <a-W> B
map -docstring "big word"                       global object b     <a-w>
map global object <a-w> ': nop<ret>'

# Pasting.
map -docstring "paste before cursor"        global normal <a-p> P
map -docstring "paste after each selection" global normal P     <a-p>

# q
map -docstring "record a macro" global normal <a-q> Q

# o
map -docstring "insert on new line above cursor" global normal <a-o> O
map -docstring "insert new line below cursor"    global normal O     <a-o>

# z
map -docstring "recall selections from register"                        global normal <a-z> Z
map -docstring "combine current selections with selections in register" global normal Z     <a-z>

# u
map -docstring "redo last change"         global normal <a-u> U
map -docstring "move forwards in history" global normal U     <a-u>

# i and object selection
map -docstring "insert after the cursor"        global normal <a-i> a
map -docstring "insert at the end of the line"  global normal <a-I> A
map -docstring "enter object selection (outer)" global normal A     <a-a>
map -docstring "enter object selection (inner)" global normal a     <a-i>

map global normal <a-a> ': nop<ret>'
map global normal <a-A> ': nop<ret>'

# s
map -docstring "Split selection"            global normal <a-s> S
map -docstring "Split selection into lines" global normal S     <a-s>

## Only use j and l (gasp!)
#map global normal h ': nop<ret>'
#map global normal k ': nop<ret>'
#map global normal K ': nop<ret>'
#map global normal H ': nop<ret>'
#
#map global normal <a-j> k
#map global normal <a-l> h
#map global normal <a-J> K
#map global normal <a-L> H
#
#map global goto h '<esc>:nop<ret>'
#map global goto <a-l> h
#map global goto k '<esc>:nop<ret>'
#map global goto <a-j> k
