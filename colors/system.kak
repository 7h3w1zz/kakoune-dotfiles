evaluate-commands %sh{
echo "
    # Code highlighting
    face global value         rgb:${BASE0E}
    face global type          yellow
    face global variable      rgb:${BASE0C}
    face global module        green
    face global function      white
    face global string        green
    face global keyword       red
    face global operator      white
    face global attribute     rgb:${BASE09}
    face global comment       rgb:${BASE03}+i
    face global documentation comment
    face global meta          rgb:${BASE0D}
    face global builtin       white+b

    # Markdown highlighting
    face global title     green+b
    face global header    rgb:${BASE09}
    face global mono      rgb:${BASE04}
    face global block     rgb:${BASE0D}
    face global link      rgb:${BASE0C}+u
    face global bullet    yellow
    face global list      white

    face global Default            white,black
    face global PrimarySelection   rgba:${BASE00}A0,rgb:${BASE04}+g
    face global SecondarySelection rgba:${BASE05}A0,rgb:${BASE02}+g
    face global PrimaryCursor      rgba:${BASE00}A0,white+g
    face global SecondaryCursor    rgba:${BASE05}A0,rgb:${BASE03}+g
    face global PrimaryCursorEol   PrimaryCursor
    face global SecondaryCursorEol SecondaryCursor
    face global LineNumbers        rgb:${BASE03}
    face global LineNumberCursor   yellow,rgb:${BASE01}
    face global LineNumbersWrapped rgb:${BASE01}
    face global MenuForeground     ,rgb:${BASE02}
    face global MenuBackground     ,rgb:${BASE01}
    face global MenuInfo           black
    face global Information        yellow
    face global Error              black,red
    face global StatusLine         ,rgb:${BASE01}
    face global StatusLineMode     yellow+b
    face global StatusLineInfo     rgb:${BASE0E}
    face global StatusLineValue    red
    face global StatusCursor       black,white
    face global Prompt             yellow
    face global MatchingChar       white,rgb:${BASE03}+b
    face global BufferPadding      rgb:${BASE02}
    face global Whitespace         black+f
"
}
