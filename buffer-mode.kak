# Buffer management mode
declare-user-mode buffer
map -docstring "next buffer"         global buffer n ': buffer-next<ret>'
map -docstring "previous buffer"     global buffer p ': buffer-previous<ret>'
map -docstring "go to buffer"        global buffer b ': prompt -buffer-completion "Go to buffer:" %{buffer %val{text}}<ret>'
map -docstring "delete buffer"       global buffer d ': delete-buffer<ret>'
map -docstring "format buffer"       global buffer f ': format-buffer<ret>'
map -docstring "lint buffer"         global buffer l ': lint-buffer<ret>'
map -docstring "new scratch buffer"  global buffer s ': e -scratch<ret>'
map -docstring "buffer key bindings" global user   b ': enter-user-mode buffer<ret>'
