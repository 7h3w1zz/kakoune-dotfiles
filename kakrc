# Modules
#require-module auto-pairs
#auto-pairs-enable

# Add line numbers to buffers
#hook global BufCreate .* %{ add-highlighter buffer/ number-lines -hlcursor }
#hook global BufCreate .* %{ add-highlighter buffer/ show-whitespaces }
add-highlighter global/ number-lines -hlcursor
add-highlighter global/ show-whitespaces

# enable lsp
eval %sh{kak-lsp --kakoune --session $kak_session}
hook global WinSetOption filetype=(c|cpp|haskell) %{ lsp-enable-window; lsp-auto-hover-enable }
hook global BufSetOption filetype=(haskell) %{ map -docstring "run suggestion code action" buffer lsp A ': lsp-code-action "Apply hint:"<ret>' }
map -docstring "language server commands" global user l ': enter-user-mode lsp<ret>'
set-face global DiagnosticError   ,,red+c
set-face global DiagnosticWarning ,,yellow+c
set-face global DiagnosticHint    +c
set-face global DiagnosticInfo    +u

# HTML formatting
hook global BufSetOption filetype=html %{ set buffer formatcmd 'htmlq --pretty' }

# Markdown formatting
hook global BufSetOption filetype=markdown %{ set buffer formatcmd 'prettier --parser markdown' }

# Phantom selection face
face global PhantomSelection %sh{echo "default,rgb:${BASE01}+g"}

# User mode utility mappings
map -docstring "open with default app"    global user o '<a-|>xargs xdg-open<ret>'
map -docstring "edit kakrc"               global user e ': e ~/.config/kak/kakrc<ret>'
map -docstring "source current selection" global user s ': evaluate-commands %reg{.}<ret>'
map -docstring "quick align"              global user a 's("[^\n"]+"|''[^\n'']+'')|\S+<ret><a-:><a-;>&'
map -docstring "wrap prose"               global user w ': toggle-wrap<ret>'
map -docstring "format selection(s)"      global user f '| $kak_opt_formatcmd<ret>'

# Fix alt+key uniformity (always reverse direction, where it makes sense)
source "%val{config}/consistent-alt.kak"

# Firefox brain compatibility
map -docstring "next match" global normal <c-g> n
map -docstring "previous match" global normal <c-s-g> <a-n>

# Null register delete
map -docstring "delete to the null register" global normal D '"_d'

# Case-insensitive search
map -docstring "search forward (case-insensitive)"  global normal /     '/(?i)'
map -docstring "search backward (case-insensitive)" global normal <a-/> '<a-/>(?i)'

# Tab key inserts number of spaces according to tabstop
map global insert <tab> '<tab><a-;>: execute-keys -draft h@<ret>'

# Tab key does completions (only when possible)
source "%val{config}/tab-complete.kak"

# Buffer management mode
source "%val{config}/buffer-mode.kak"

### Options ###
set-option global ui_options           terminal_assistant=none
set-option global startup_info_version 20221031
set-option global tabstop              2
set-option global indentwidth          2

### Colors ###
colorscheme system

### Useful Commands ###
define-command -docstring "Toggle word wrapping on a buffer, with an optional number of characters, defaults to 80" \
               -params 0..1 -override \
               toggle-wrap %{ \
                 try %{
                   try %{ add-highlighter buffer/word-wrap wrap -word -indent -width %arg{1} } \
                   catch %{ add-highlighter buffer/word-wrap wrap -word -indent -width 80 } \
                 } \
                 catch %{ remove-highlighter buffer/word-wrap } \
               }
